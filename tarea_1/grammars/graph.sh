
GRAPH=./graphs/
GRAPHPATH=$1
PNGPATH=./graphs/${1}_graph.png

mkdir $GRAPH
dot -Tpng $GRAPHPATH -o $PNGPATH