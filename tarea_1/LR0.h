#ifndef _LR0DFAGEN_H_
#define _LR0DFAGEN_H_

#include <vector>
#include <string>
#include "grammar.h"

using namespace std;

class LR0 {
public:
    struct Item {
        Item(const Grammar::Production &prod, int dot_pos):
            prod(prod), dot_pos(dot_pos) {}

        bool operator==(const Item& rhs) {
            return (&prod == &(rhs.prod)) && (dot_pos == rhs.dot_pos);
        }

        bool operator!=(const Item& rhs) {
            return !(*this == rhs);
        }

        const Grammar::Production &prod;
        int dot_pos;
    };

    struct State;

    struct Transition {
        Transition(const std::string &symbol, State& next_state, int next_state_num):
            symbol(symbol), next_state(next_state), next_state_num(next_state_num) {}

        std::string symbol;
        State& next_state;
        int next_state_num;
    };

    struct State {
        bool operator==(const State& rhs) {
            if (items.size() != rhs.items.size()) {
                return false;
            }

            for (int i = 0; i < items.size(); i++) {
                if (items[i] != rhs.items[i]) {
                    return false;
                }
            }

            return true;
        }

        bool operator!=(const State& rhs) {
            return !(*this == rhs);
        }

        std::vector<Item> items;
        std::vector<Transition> transitions;
    };

    using DFA = std::vector<State>;

    LR0(Grammar &gr): gr(gr) {
        computeDFA();
        printDFA();

    }
    const DFA& getDFA() { return dfa; }
    const std::string getGraphVix();

private:
    void computeDFA();
    void printDFA();
    const string printState(State &s);
    const string printItem(Item &item);
    void fillState(State &s);
    int findState(std::vector<Item> &items);
    bool transitionExists(State &s, std::string follow, int next_state);

private:
    Grammar &gr;
    DFA dfa;
};

#endif
