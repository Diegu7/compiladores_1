#include "LR0.h"
#include "grammar.h"
#include <iostream>
#include <sstream>
#include <algorithm>

void LR0::computeDFA() {
	State q0, newState;
	//Item temp(gr.getRules()[0], 0);
	vector<Item> items_temp;
	q0.items.push_back(Item(gr.getRules()[0], 0));
	dfa.push_back(q0);

	for(int i = 0; i < dfa.size() /*&& i < 10*/; i++){
		//cout << "MANAGING STATE " << i << endl;
		fillState(dfa[i]);
		for(int j = 0; j < dfa[i].items.size(); j++){
			if(dfa[i].items[j].prod.rhs.size() == dfa[i].items[j].dot_pos)
				continue;

			string follow = dfa[i].items[j].prod.rhs[dfa[i].items[j].dot_pos];
			


			for(int k = 0; k < dfa[i].items.size(); k++){
				if(dfa[i].items[k].prod.rhs[dfa[i].items[k].dot_pos] == follow){
					items_temp.push_back(Item(dfa[i].items[k].prod, dfa[i].items[k].dot_pos + 1));
				}
			}

			int nextState = findState(items_temp);
			
			if(nextState != -1){
				if(!transitionExists(dfa[i], follow, nextState))
					dfa[i].transitions.push_back(Transition(follow, dfa[nextState], nextState));
			}
			else{
				newState = State();
				for(int k = 0; k < items_temp.size(); k++){
					newState.items.push_back(Item(items_temp[k].prod, items_temp[k].dot_pos));	
				}
				dfa.push_back(newState);
				nextState = findState(newState.items);
				dfa[i].transitions.push_back(Transition(follow, dfa[nextState], nextState));
			}
			items_temp.clear();
		}
	}
}

int LR0::findState(vector<Item> &items){
	int i;
	bool found = true;
	for(i = 0; i < dfa.size(); i++){
		//cout << "------- State " << i << " -------" << endl;
		for(int j = 0; j < items.size(); j++){
			//cout << "Comparing item " << j << endl;
			//cout << "---- state items ----" << endl;
			//printItem(dfa[i].items[j]);
			//cout << "---- params items ----" << endl;
			//printItem(items[j]);
			if(dfa[i].items[j] != items[j]){
				found = false;
				break;
			}
		}
		if(!found && i != dfa.size() - 1) found = true;
		else break;
	}
	//cout << "!!!!!!!!!!!!!!!Found ? " << found << endl;
	return found ? i : -1;
}

bool LR0::transitionExists(State &s, std::string symbol, int next_state){
	for(int i = 0; i < s.transitions.size(); i++){
		if(s.transitions[i].symbol == symbol && s.transitions[i].next_state_num == next_state)
			return true;
	}
	return false;
}

void LR0::fillState(State &s){
	for(int i = 0; i < s.items.size(); i++){
		if(s.items[i].prod.rhs.size() == s.items[i].dot_pos)
			continue;
		string follow = s.items[i].prod.rhs[s.items[i].dot_pos];
		if(find(gr.getNonTerminals().begin(), gr.getNonTerminals().end(), follow) != gr.getNonTerminals().end()){
			for(int j = 0; j < gr.getRules().size(); j++){
				if(follow == gr.getRules()[j].lhs && follow != s.items[i].prod.lhs){
					int d_pos = gr.getRules()[j].rhs[0] == "''" ? 1 : 0;
					s.items.push_back(Item(gr.getRules()[j], d_pos));
				}
			}
		}
	}
}

const string LR0::getGraphVix(){
	ostringstream res;
	res << "digraph {\n";
	for(int i = 0; i < dfa.size(); i++){
		res << "\tq" << i << " [label=\"";
		for(int j = 0; j < dfa[i].items.size(); j++){
			res << printItem(dfa[i].items[j]) << "\\n";
		}
		res << "\"];" << endl;
	}
	for(int i = 0; i < dfa.size(); i++){
		for(int j = 0; j < dfa[i].transitions.size(); j++){
			res << "\tq" << i << " -> q" << dfa[i].transitions[j].next_state_num;
			res << "[label=\"" << dfa[i].transitions[j].symbol << "\"];" << endl;
		}
	}
	res << "}";
	return res.str();
}

void LR0::printDFA(){
	for(int i = 0; i < dfa.size(); i++){
		cout << "State: " << i << endl;
		cout << printState(dfa[i]);
	}
	cout << getGraphVix() << endl;
}

const string LR0::printState(State &s){
	ostringstream res;
	for(int i = 0; i < s.items.size(); i++)
		res << printItem(s.items[i]) << endl;
	return res.str();
}

const string LR0::printItem(Item &item){
	ostringstream res;
	res << item.prod.lhs << " -> ";
	for(int j = 0; j < item.prod.rhs.size(); j++){
		if(item.dot_pos == j)
			res << ".";
		res << item.prod.rhs[j] << " ";
	}
	if(item.dot_pos == item.prod.rhs.size())
		res << ".";
	return res.str();
}