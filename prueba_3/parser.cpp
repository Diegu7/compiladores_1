#include "parser.h"


void Parser::parse() {
	vector<Token> pila;

	pila.push_back(Token::Eof);
	pila.push_back(Token::input);
	token = lexer.getNextToken();

	while(true){
		int last = pila.size() - 1;
		Token temp = pila.at(last);
		pila.pop_back();

		vector<Token> grammar = getProduction(temp, token);

		if(lexer.isTerminal(temp)){
			if(token != temp)
				throw "Expected something else found " + lexer.getText() + " instead";

			if(temp == Token::Eof)
				break;
			
			cout << lexer.getText();
			token = lexer.getNextToken();
		}
		else{

			if(grammar.empty())
				throw "Expected something else got " + lexer.getText() + " instead";

			for(int i = grammar.size() - 1; i >= 0; i--){
				if(grammar.at(i) != Token::Epsilon)
					pila.push_back(grammar.at(i));
			}
		}
	}

	cout << endl;
	
	if(token != Token::Eof)
		throw "Expected EOF got " + lexer.getText() + " instead";
}


vector<Token> Parser::getProduction(Token nonTerminal, Token terminal){
	vector<Token> result;
    switch(nonTerminal){
        case Token::input:
        	if(terminal == Token::Ident || terminal == Token::KwPrint){
        		result.push_back(Token::stlist);
        	}
            break;
        case Token::stlist:
        	if(terminal == Token::Ident || terminal == Token::KwPrint){
        		result.push_back(Token::st);
        		result.push_back(Token::Semicolon);
        		result.push_back(Token::stlistp);
        	}
        	break;
        case Token::stlistp:
        	if(terminal == Token::Ident || terminal == Token::KwPrint){
        		result.push_back(Token::st);
        		result.push_back(Token::Semicolon);
        		result.push_back(Token::stlistp);
        	}
        	else if(terminal == Token::Eof){
        		result.push_back(Token::Epsilon);
        	}
        	break;
        case Token::st:
        	if(terminal == Token::Ident){
        		result.push_back(Token::Ident);
        		result.push_back(Token::OpAssign);
        		result.push_back(Token::exp);
        	}
        	else if(terminal == Token::KwPrint){
        		result.push_back(Token::KwPrint);
        		result.push_back(Token::exp);
        	}
        	break;
        case Token::exp:
        	if(terminal == Token::OpenPar || terminal == Token::Number || terminal == Token::Ident){
        		result.push_back(Token::term);
        		result.push_back(Token::expp);
        	}
        	break;
        case Token::expp:
        	if(terminal == Token::OpAdd){
        		result.push_back(Token::OpAdd);
        		result.push_back(Token::term);
        		result.push_back(Token::expp);
        	}
        	else if(terminal == Token::Semicolon || terminal == Token::Ident || terminal == Token::KwPrint || terminal == Token::ClosePar || terminal == Token::OpAdd || terminal == Token::Eof){
        		result.push_back(Token::Epsilon);
        	}
        	break;
        case Token::term:
        	if(terminal == Token::Ident || terminal == Token::Number || terminal == Token::OpenPar){
        		result.push_back(Token::factor);
        		result.push_back(Token::termp);
        	}
        	break;
        case Token::termp:
        	if(terminal == Token::OpMult){
        		result.push_back(Token::OpMult);
        		result.push_back(Token::factor);
        		result.push_back(Token::termp);
        	}
        	else if(terminal == Token::Semicolon || terminal == Token::Ident || terminal == Token::KwPrint || terminal == Token::ClosePar || terminal == Token::OpAdd || terminal == Token::Eof){
        		result.push_back(Token::Epsilon);
        	}
        	break;
        case Token::factor:
        	if(terminal == Token::Ident){
        		result.push_back(Token::Ident);
        	}
        	else if(terminal == Token::OpenPar){
        		result.push_back(Token::OpenPar);
        		result.push_back(Token::exp);
        		result.push_back(Token::ClosePar);
        	}
        	else if(terminal == Token::Number){
        		result.push_back(Token::Number);
        	}
        	break;
    }
    return result;
}
