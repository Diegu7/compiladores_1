#ifndef _PARSER_H
#define _PARSER_H

#include "lexer.h"
#include <vector>

using namespace std;

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {}
    void parse();

private:

	vector<Token> getProduction(Token nonTerminal, Token terminal);

    Lexer& lexer;
    Token token;
};

#endif
