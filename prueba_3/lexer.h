#ifndef _LEXER_H
#define _LEXER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

enum class Token : int { 
    KwPrint = 0,
    Ident = 1,
    Semicolon = 2,
    OpenPar = 3, 
    ClosePar = 4, 
    OpAssign = 5,
    OpAdd = 6, 
    OpMult = 7, 
    Number = 8,
    Unknown = -2, 
    Eof = -1,
    Epsilon = -3,


    input = 20,
    stlist = 21,
    stlistp = 22,
    st = 23,
    exp = 24,
    expp = 25,
    term = 26,
    termp = 27,
    factor = 28,




};

using TokenInfo = std::pair<Token, std::string>;

class Lexer {
public:
	Lexer(const std::vector<TokenInfo>& tklst): tklist(tklst) {
        it = tklist.begin();
    }

    Token getNextToken() {
        Token tk = it->first;
        text = it->second;
        
        if (it != tklist.end()) {
            it++;
        }
        
        return tk;
    }
    
    bool hasTokens() { return it != tklist.end(); }
    std::string getText() { return text; }

    bool isTerminal(Token t){
        return (int) t < 20;
    }

private:
    std::string text;
    std::vector<TokenInfo> tklist;
    std::vector<TokenInfo>::iterator it;
};
#endif
