#ifndef _AST_H
#define _AST_H

#include <string>
#include <iostream>
#include <stdio.h>
#include <memory>

using namespace std;



#define DEFINE_EXPR(name, operStr) \
		class name##Expr : public BinaryExp{	\
		public:	\
			~name##Expr(){};\
			name##Expr(ASTN_ptr exp1, ASTN_ptr exp2) : BinaryExp (move(exp1), move(exp2)){prec = exp1->prec + 1;}; \
			string getOper() override {return operStr;};\
			string toString() override {return "(" + exp1->toString() + ")" + operStr + "(" + exp2->toString() + ")";};\
		};

class ASTNode{
	public:
		virtual string toString() = 0;
		int prec;
};

using ASTN_ptr = ASTNode*;

class BinaryExp : public ASTNode{
	public:
		BinaryExp(ASTN_ptr exp1, ASTN_ptr exp2) : exp1(move(exp1)), exp2(move(exp2)){};
		~BinaryExp(){};
		virtual string getOper() = 0;
		ASTN_ptr exp1;
		ASTN_ptr exp2;
};

class NumExpr : public ASTNode{
	public:
		NumExpr(int value) : value(value){prec = 0;};
		int value;
		string toString() override {return to_string(value);};
};

class IdExpr : public ASTNode{
	public:
		IdExpr(string value) : value(value){prec = 0;};
		string value;
		string toString() override {return value;};
};

DEFINE_EXPR(Add, "+");
DEFINE_EXPR(Sub, "-");
DEFINE_EXPR(Mul, "*");
DEFINE_EXPR(Div, "/");
DEFINE_EXPR(Mod, "%");




#endif
