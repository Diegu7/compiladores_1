#include "parser.h"

void Parser::parse() {
	token = lexer.getNextToken();
	input();
}

void Parser::input(){
	stmt_list();
}

void Parser::stmt_list(){
	stmt();
	stmt_listp();
}

void Parser::stmt_listp(){
	if(token == Token::Semicolon){
		token = lexer.getNextToken();
		stmt();
		stmt_listp();
	}
}

void Parser::stmt(){
	if(token == Token::Ident){
		token = lexer.getNextToken();
		if(token != Token::OpAssign)
			throw "Expected = got " + lexer.getText();
		token = lexer.getNextToken();
		expr();
	}
	else if(token == Token::KwPrint){
		token = lexer.getNextToken();
		expr();
	}
	else if(token == Token::Eof){

	}
	else
		throw "Expected IDENT or PRINT got " + lexer.getText();
}

void Parser::expr(){
	term();
	exprp();
}

void Parser::exprp(){
	if(token == Token::OpAdd){
		token = lexer.getNextToken();
		term();
		exprp();
	}
}

void Parser::term(){
	factor();
	termp();
}

void Parser::termp(){
	if(token == Token::OpMult){
		token = lexer.getNextToken();
		factor();
		termp();
	}
}

void Parser::factor(){
	if(token == Token::Ident || token == Token::Number){
		token = lexer.getNextToken();
	}
	else if(token == Token::OpenPar){
		token = lexer.getNextToken();
		expr();
		if(token != Token::ClosePar)
			throw "Expected ) got " + lexer.getText();
		token = lexer.getNextToken();
	}
	else
		throw "Expected IDENT, NUM, or ( got " + lexer.getText();
}
