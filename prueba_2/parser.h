#ifndef _PARSER_H
#define _PARSER_H

#include "lexer.h"

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {}
    void parse();

private:

	void input();
	void stmt_list();
	void stmt_listp();
	void stmt();
	void expr();
	void exprp();
	void term();
	void termp();
	void factor();

    Lexer& lexer;
    Token token;
};

#endif
