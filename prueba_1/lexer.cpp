#include "lexer.h"

Token Lexer::getNextToken() {
    int currState = 0;
    char currChar = in.get();
    text = "";
    while(true){
    	switch(currState){
    		case 0:
    			if(currChar == EOF)
    				return Token::Eof;
    			if(currChar == ' ' || currChar == '\n' || currChar == '\t'){
    				currChar = in.get();
    				continue;
    			}
    			if(currChar == '0' || currChar == '1'){
    				currState = 1;
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else{
    				currState = 3;
    			}
    			break;
    		case 1:
    			if(currChar == '0' || currChar == '1'){
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else if(currChar == 'b' || currChar == 'B'){
    				currState = 2;
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else{
    				currState = 4;
    			}
    			break;
    		case 2:
    			return Token::Binary;
    			break;
    		case 3:
    			if(currChar >= '2' && currChar <= '7'){
    				currState = 4;
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else{
    				currState = 6;
    			}
    			break;
    		case 4:
    			if(currChar >= '2' && currChar <= '7'){
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else if(currChar == 'o' || currChar == 'O'){
    				currState = 5;
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else{
    				currState = 7;
    			}
    			break;
    		case 5:
    			return Token::Octal;
    			break;
    		case 6:
    			if(isdigit(currChar)){
    				currState = 7;
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else{
    				currState = 9;
    			}
    			break;
    		case 7:
    			if(isdigit(currChar)){
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else if(currChar == 'h' || currChar == 'H'){
    				currState = 10;
    			}
    			else{
    				currState = 8;
    			}
    			break;
    		case 8:
    			return Token::Decimal;
    			break;
    		case 9:
    			if((currChar >= 'a' && currChar <= 'f') || (currChar >= 'A' && currChar <= 'F')){
    				currState = 10;
    				text = text + currChar;
    				currChar = in.get();
    			}
    			else{
    				currState = 12;
    			}
    			break;
    		case 10:
    			if((currChar >= 'a' && currChar <= 'f') || (currChar >= 'A' && currChar <= 'F') || isdigit(currChar)){
    				text = text + currChar;
    				currChar = in.get();	
    			}
    			else if(currChar == 'h' || currChar == 'H'){
    				text = text + currChar;
    				currChar = in.get();
    				currState = 11;
    			}
    			break;
    		case 11:
    			return Token::Hex;
    			break;
    		case 12:
    			return Token::Eof;
    	}
    }
}